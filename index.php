<?php
session_start();

set_include_path(get_include_path()
    .PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/controllers'
    .PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/models'
    .PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/views');
require_once 'init.php';
spl_autoload_register(function ($class) {
    include_once $class.'.php';
});
$front = \controllers\FrontController::getInstance();
$front->route();
echo $front->getBody();

