<?php
namespace components;

/**
 * Class Service Helper
 * @package components
 */
class Service
{
	public static function clearStr($str): string
	{
		return htmlentities(strip_tags(trim($str)));
	}
	
	public static function clearInt($data){
		return (int)$data;
	}
	
}