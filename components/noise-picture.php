<?php
session_start();
error_reporting(1);
$img = imagecreatefromjpeg("../style/noise.jpg");
imageAntiAlias($img, true);
$captcha = '';
$test = substr(md5(uniqid()), 0, 2);
for($i=0, $x=20, $y=30; $i<2; $i++){
    $upper = rand(0,1);
    ($upper == 1) ? $char = randChar(): $char = mb_strtolower(randChar());
	$color = imagecolorallocate($img, 128, 0, 0);
    imageTtfText($img, rand(24,30), rand(-40,40), $x, $y, $color ,realpath('../style/font/bellb.ttf'), $char);
    $x+=40;
    $captcha .= $char;
}
$_SESSION['captha'] = $captcha;
function randChar(){
    $rand = rand(65,90);
    return chr($rand);
}
function random_color_part() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}
function random_color() {
    return random_color_part() . random_color_part() . random_color_part();
}
header("Content-type: image/jpg");
imageJpeg($img);