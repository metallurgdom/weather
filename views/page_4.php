<?php
$messages = $this->getMessages();

?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="../style/_css/jquery-ui.min.css">
    <!-- Bootstrap -->
    <link href="../style/_css/bootstrap.min.css" rel="stylesheet">
    <script src="../style/js/jquery.js"></script>
    <script src="../style/js/jquery-ui.min.js"></script>
    <script src="../style/js/jquery.validate.min.js"></script>
    <style>
        div.row {
            margin-bottom: 20px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="jumbotron">
        <h1 style="text-align: center">Сообщения</h1>
        <h5><a href="/parse">На главную</a> / <a href="/contact">Обратная связь</a></h5>
    </div>
	<?php if ($_SESSION[ 'userName' ]) : ?>
	<?php if (isset($messages) and !empty($messages)): ?>
		<?php foreach ($messages as $item): ?>
            <div class="row">
                <div class="col-sm-4">
                    <ul>
                        <li style="list-style: none; font-size: 20px;"><?= $item[ 'name' ] ?></li>
                    </ul>
                </div>
                <div class="col-sm-8"
                     style="padding:20px; text-align: left; background-color: #d4d2c9; border-radius: 10px;">
                    <p><?= $item[ 'messages' ] ?></p>
                </div>
            </div>
		<?php endforeach; ?>
	<?php else: ?>
        <h2>Еще нет ни одного сообщения</h2>
	<?php endif; ?>
</div>
<?php else: ?>
    <h1>Для просмотра необходима <a href="/parse">авторизация</a></h1>
<?php endif; ?>
<script src="../style/js/bootstrap.min.js"></script>
</body>
</html>
