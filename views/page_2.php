<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>My Website</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="../style/_css/bootstrap.min.css" rel="stylesheet">
    <style>

        .nav.nav-tabs a {
            padding: 30px 60px;
        }

        .nav.nav-tabs img {
            position: absolute;
            top: 10px;
            left: 10px;
        }

        .nav.nav-tabs p {
            padding: 0 5px;
        }

        .nav.nav-tabs span {
            margin-left: 5px;
        }

        /************/
        .wicon {
            background: url(../style/weather.png) no-repeat;
        }

        dl.wind {
            display: inline;
            position: relative;
            padding-left: 20px;
        }

        dl.wind dt {
            position: absolute;
            left: 0;
            top: -4px;
            width: 17px;
            padding-top: 18px;
            font-size: 9px;
            font-weight: normal;
            line-height: 11px;
            text-align: center;
        }

        dl.wind dd {
            display: inline;
        }

        dl.wind .wind0 {
            background-position: 100% -120px;
        }

        dl.wind .wind1 {
            background-position: 100% -160px;
        }

        dl.wind .wind5 {
            background-position: 100% -200px;
        }

        dl.wind .wind3 {
            background-position: 100% -240px;
        }

        dl.wind .wind7 {
            background-position: 100% -280px;
        }

        dl.wind .wind2 {
            background-position: 100% -320px;
        }

        dl.wind .wind8 {
            background-position: 100% -360px;
        }

        dl.wind .wind6 {
            background-position: 100% -400px;
        }

        dl.wind .wind4 {
            background-position: 100% -440px;
        }

        tr td:nth-child(1) {
            font-size: 16px;
            font-weight: bold;
            color: #004d9a;
        }

        tr td:nth-child(3) {
            width: 90px;
        }

        tr td:nth-child(4) {
            width: 45px;
            font-size: 26px;
            color: #003b77;
            font-weight: bold;
        }

        table tr td {
            text-align: center;
        }

        table th {
            font-size: 12px;
        }

        .nav.nav-tabs.nav-justified p {
            font-weight: bold;
            font-size: 20px;
        }

        div.col-lg-12 table td {
            text-align: center;
            vertical-align: baseline;
        !important;
        }

    </style>
</head>
<body>
<div class="container">
    <div class="jumbotron">
        <h1 style="text-align: center">Погода в Запорожье</h1>
        <h5><a href="/parse">На главную</a> / <a href="/contact">Обратная связь</a> / <a href="/contact/messages">Комментарии</a>
        </h5>
    </div>
	<?php if ($_SESSION[ 'userName' ]) : ?>
    <div class="row">
        <div class="col-lg-12">
            <ul class="nav nav-tabs nav-justified">
                <li role="presentation" class="active">
                    <a href="#1" data-toggle="tab">
                        <img class="cloudness png" src="<?= $this->img_top[ 1 ][ 0 ] ?>" alt="" width="55" height="55">
                        <p>
                            <span><?= $this->date_day_top[ 1 ][ 0 ] ?></span><span><?= $this->date_day_top[ 2 ][ 0 ] ?></span>
                        </p>
                        <p><span><?= $this->temp_top_1[ 1 ][ 0 ] ?>
                                ...</span><span><?= $this->temp_top_2[ 1 ][ 0 ] ?></span>
                        </p>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#2" data-toggle="tab">
                        <img class="cloudness png" src="<?= $this->img_top[ 1 ][ 1 ] ?>" alt="" width="55" height="55">

                        <p>
                            <span><?= $this->date_day_top[ 1 ][ 1 ] ?></span><span><?= $this->date_day_top[ 2 ][ 1 ] ?></span>
                        </p>

                        <p><span><?= $this->temp_top_1[ 1 ][ 1 ] ?>
                                ...</span><span><?= $this->temp_top_2[ 1 ][ 1 ] ?></span>
                        </p>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#3" data-toggle="tab">
                        <img class="cloudness png" src="<?= $this->img_top[ 1 ][ 2 ] ?>" alt="" width="55" height="55">

                        <p>
                            <span><?= $this->date_day_top[ 1 ][ 2 ] ?></span><span><?= $this->date_day_top[ 2 ][ 2 ] ?></span>
                        </p>

                        <p><span><?= $this->temp_top_1[ 1 ][ 2 ] ?>
                                ...</span><span><?= $this->temp_top_2[ 1 ][ 2 ] ?></span>
                        </p>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="1">
                    <table class="table table-hover">
                        <tr>
                            <th colspan="3">Характеристики погоды, атмосферные явления</th>
                            <th>Tемпература воздуха, °C</th>
                            <th>Атм. давл., мм рт. ст.</th>
                            <th>Ветер,<br>м/с</th>
                            <th>Влажность <br>воздуха,%</th>
                            <th><p>Ощущается,°C</p></th>
                        </tr>
                        <tr>
                            <td>Ночь</td>
                            <td><img class="png" src="<?= $this->row_weather_img[ 1 ][ 0 ] ?>" alt="Ясно" title="Ясно"
                                     width="55" height="55"></td>
                            <td><span><?= $this->row_text[ 1 ][ 0 ] ?></span></td>
                            <td><span><?= $this->row_temp[ 1 ][ 0 ] ?></span></td>
                            <td><span><?= $this->row_pressure[ 1 ][ 0 ] ?></span></td>
                            <td>
                                <dl class="wind"><?= $this->row_wind[ 1 ][ 0 ] ?></dd></dl>
                            </td>
                            <td><span><?= $this->row_humidity[ 1 ][ 0 ] ?></span></td>
                            <td><span><?= $this->row_feeling[ 1 ][ 0 ] ?></span></td>
                        </tr>
                        <tr>
                            <td>Утро</td>
                            <td><img class="png" src="<?= $this->row_weather_img[ 1 ][ 1 ] ?>" alt="Ясно" title="Ясно"
                                     width="55" height="55"></td>
                            <td><span><?= $this->row_text[ 1 ][ 1 ] ?></span></td>
                            <td><span><?= $this->row_temp[ 1 ][ 1 ] ?></span></td>
                            <td><span><?= $this->row_pressure[ 1 ][ 1 ] ?></span></td>
                            <td>
                                <dl class="wind"><?= $this->row_wind[ 1 ][ 1 ] ?></dd></dl>
                            </td>
                            <td><span><?= $this->row_humidity[ 1 ][ 1 ] ?></span></td>
                            <td><span><?= $this->row_feeling[ 1 ][ 1 ] ?></span></td>
                        </tr>
                        <tr>
                            <td>День</td>
                            <td><img class="png" src="<?= $this->row_weather_img[ 1 ][ 2 ] ?>" alt="Ясно" title="Ясно"
                                     width="55" height="55"></td>
                            <td><span><?= $this->row_text[ 1 ][ 2 ] ?></span></td>
                            <td><span><?= $this->row_temp[ 1 ][ 2 ] ?></span></td>
                            <td><span><?= $this->row_pressure[ 1 ][ 2 ] ?></span></td>
                            <td>
                                <dl class="wind"><?= $this->row_wind[ 1 ][ 2 ] ?></dd></dl>
                            </td>
                            <td><span><?= $this->row_humidity[ 1 ][ 2 ] ?></span></td>
                            <td><span><?= $this->row_feeling[ 1 ][ 2 ] ?></span></td>
                        </tr>
                        <tr>
                            <td>Вечер</td>
                            <td><img class="png" src="<?= $this->row_weather_img[ 1 ][ 3 ] ?>" alt="Ясно" title="Ясно"
                                     width="55" height="55"></td>
                            <td><span><?= $this->row_text[ 1 ][ 3 ] ?></span></td>
                            <td><span><?= $this->row_temp[ 1 ][ 3 ] ?></span></td>
                            <td><span><?= $this->row_pressure[ 1 ][ 3 ] ?></span></td>
                            <td>
                                <dl class="wind"><?= $this->row_wind[ 1 ][ 3 ] ?></dd></dl>
                            </td>
                            <td><span><?= $this->row_humidity[ 1 ][ 3 ] ?></span></td>
                            <td><span><?= $this->row_feeling[ 1 ][ 3 ] ?></span></td>
                        </tr>
                    </table>
                </div>
                <div class="tab-pane fade" id="2">
                    <table class="table table-hover">
                        <tr>
                            <th colspan="3">Характеристики погоды, атмосферные явления</th>
                            <th>Tемпература воздуха, °C</th>
                            <th>Атм. давл., мм рт. ст.</th>
                            <th>Ветер,<br>м/с</th>
                            <th>Влажность <br>воздуха,%</th>
                            <th><p>Ощущается,°C</p></th>
                        </tr>
                        <tr>
                            <td>Ночь</td>
                            <td><img class="png" src="<?= $this->row_weather_img[ 1 ][ 4 ] ?>" alt="Ясно" title="Ясно"
                                     width="55" height="55"></td>
                            <td><span><?= $this->row_text[ 1 ][ 4 ] ?></span></td>
                            <td><span><?= $this->row_temp[ 1 ][ 4 ] ?></span></td>
                            <td><span><?= $this->row_pressure[ 1 ][ 4 ] ?></span></td>
                            <td>
                                <dl class="wind"><?= $this->row_wind[ 1 ][ 4 ] ?></dd></dl>
                            </td>
                            <td><span><?= $this->row_humidity[ 1 ][ 4 ] ?></span></td>
                            <td><span><?= $this->row_feeling[ 1 ][ 4 ] ?></span></td>
                        </tr>
                        <tr>
                            <td>Утро</td>
                            <td><img class="png" src="<?= $this->row_weather_img[ 1 ][ 5 ] ?>" alt="Ясно" title="Ясно"
                                     width="55" height="55"></td>
                            <td><span><?= $this->row_text[ 1 ][ 5 ] ?></span></td>
                            <td><span><?= $this->row_temp[ 1 ][ 5 ] ?></span></td>
                            <td><span><?= $this->row_pressure[ 1 ][ 5 ] ?></span></td>
                            <td>
                                <dl class="wind"><?= $this->row_wind[ 1 ][ 5 ] ?></dd></dl>
                            </td>
                            <td><span><?= $this->row_humidity[ 1 ][ 5 ] ?></span></td>
                            <td><span><?= $this->row_feeling[ 1 ][ 5 ] ?></span></td>
                        </tr>
                        <tr>
                            <td>День</td>
                            <td><img class="png" src="<?= $this->row_weather_img[ 1 ][ 6 ] ?>" alt="Ясно" title="Ясно"
                                     width="55" height="55"></td>
                            <td><span><?= $this->row_text[ 1 ][ 6 ] ?></span></td>
                            <td><span><?= $this->row_temp[ 1 ][ 6 ] ?></span></td>
                            <td><span><?= $this->row_pressure[ 1 ][ 6 ] ?></span></td>
                            <td>
                                <dl class="wind"><?= $this->row_wind[ 1 ][ 6 ] ?></dd></dl>
                            </td>
                            <td><span><?= $this->row_humidity[ 1 ][ 6 ] ?></span></td>
                            <td><span><?= $this->row_feeling[ 1 ][ 6 ] ?></span></td>
                        </tr>
                        <tr>
                            <td>Вечер</td>
                            <td><img class="png" src="<?= $this->row_weather_img[ 1 ][ 7 ] ?>" alt="Ясно" title="Ясно"
                                     width="55" height="55"></td>
                            <td><span><?= $this->row_text[ 1 ][ 7 ] ?></span></td>
                            <td><span><?= $this->row_temp[ 1 ][ 7 ] ?></span></td>
                            <td><span><?= $this->row_pressure[ 1 ][ 7 ] ?></span></td>
                            <td>
                                <dl class="wind"><?= $this->row_wind[ 1 ][ 7 ] ?></dd></dl>
                            </td>
                            <td><span><?= $this->row_humidity[ 1 ][ 7 ] ?></span></td>
                            <td><span><?= $this->row_feeling[ 1 ][ 7 ] ?></span></td>
                        </tr>
                    </table>
                </div>
                <div class="tab-pane fade" id="3">
                    <table class="table table-hover">
                        <tr>
                            <th colspan="3">Характеристики погоды, атмосферные явления</th>
                            <th>Tемпература воздуха, °C</th>
                            <th>Атм. давл., мм рт. ст.</th>
                            <th>Ветер,<br>м/с</th>
                            <th>Влажность <br>воздуха,%</th>
                            <th><p>Ощущается,°C</p></th>
                        </tr>
                        <tr>
                            <td>Ночь</td>
                            <td><img class="png" src="<?= $this->row_weather_img[ 1 ][ 8 ] ?>" alt="Ясно" title="Ясно"
                                     width="55" height="55"></td>
                            <td><span><?= $this->row_text[ 1 ][ 8 ] ?></span></td>
                            <td><span><?= $this->row_temp[ 1 ][ 8 ] ?></span></td>
                            <td><span><?= $this->row_pressure[ 1 ][ 8 ] ?></span></td>
                            <td>
                                <dl class="wind"><?= $this->row_wind[ 1 ][ 8 ] ?></dd></dl>
                            </td>
                            <td><span><?= $this->row_humidity[ 1 ][ 8 ] ?></span></td>
                            <td><span><?= $this->row_feeling[ 1 ][ 8 ] ?></span></td>
                        </tr>
                        <tr>
                            <td>Утро</td>
                            <td><img class="png" src="<?= $this->row_weather_img[ 1 ][ 9 ] ?>" alt="Ясно" title="Ясно"
                                     width="55" height="55"></td>
                            <td><span><?= $this->row_text[ 1 ][ 9 ] ?></span></td>
                            <td><span><?= $this->row_temp[ 1 ][ 9 ] ?></span></td>
                            <td><span><?= $this->row_pressure[ 1 ][ 9 ] ?></span></td>
                            <td>
                                <dl class="wind"><?= $this->row_wind[ 1 ][ 9 ] ?></dd></dl>
                            </td>
                            <td><span><?= $this->row_humidity[ 1 ][ 9 ] ?></span></td>
                            <td><span><?= $this->row_feeling[ 1 ][ 9 ] ?></span></td>
                        </tr>
                        <tr>
                            <td>День</td>
                            <td><img class="png" src="<?= $this->row_weather_img[ 1 ][ 10 ] ?>" alt="Ясно" title="Ясно"
                                     width="55" height="55"></td>
                            <td><span><?= $this->row_text[ 1 ][ 10 ] ?></span></td>
                            <td><span><?= $this->row_temp[ 1 ][ 10 ] ?></span></td>
                            <td><span><?= $this->row_pressure[ 1 ][ 10 ] ?></span></td>
                            <td>
                                <dl class="wind"><?= $this->row_wind[ 1 ][ 10 ] ?></dd></dl>
                            </td>
                            <td><span><?= $this->row_humidity[ 1 ][ 10 ] ?></span></td>
                            <td><span><?= $this->row_feeling[ 1 ][ 10 ] ?></span></td>
                        </tr>
                        <tr>
                            <td>Вечер</td>
                            <td><img class="png" src="<?= $this->row_weather_img[ 1 ][ 11 ] ?>" alt="Ясно" title="Ясно"
                                     width="55" height="55"></td>
                            <td><span><?= $this->row_text[ 1 ][ 11 ] ?></span></td>
                            <td><span><?= $this->row_temp[ 1 ][ 11 ] ?></span></td>
                            <td><span><?= $this->row_pressure[ 1 ][ 11 ] ?></span></td>
                            <td>
                                <dl class="wind"><?= $this->row_wind[ 1 ][ 11 ] ?></dd></dl>
                            </td>
                            <td><span><?= $this->row_humidity[ 1 ][ 11 ] ?></span></td>
                            <td><span><?= $this->row_feeling[ 1 ][ 11 ] ?></span></td>
                        </tr>
                    </table>
                </div>
            </div><!--close tab-content-->
        </div>
    </div><!--close row-->
</div><!--close container-->
<?php else: ?>
    <h1>Для просмотра необходима <a href="/parse">авторизация</a></h1>
<?php endif; ?>
<!-- javascript -->
<script src="../style/js/jquery.js"></script>
<script src="../style/js/bootstrap.min.js"></script>
</body>
</html>
