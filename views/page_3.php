<?php
$output = '';
if ($_SERVER[ 'REQUEST_METHOD' ] == 'POST') {
	if (!isset($_SESSION[ 'captha' ])) {
		$output = "Ошибка отображения картинки";
	} else {
		$answer = strip_tags(trim(strtolower($_POST[ 'answer' ])));
		$answer_out = $_SESSION[ 'captha' ];
	}
}
?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="../style/_css/jquery-ui.min.css">
    <!-- Bootstrap -->
    <link href="../style/_css/bootstrap.min.css" rel="stylesheet">
    <script src="../style/js/jquery.js"></script>
    <script src="../style/js/jquery-ui.min.js"></script>
    <script src="../style/js/jquery.validate.min.js"></script>
    <style>
        #email-error, #name-error, #ctextarea-error {
            position: absolute;
            top: 3px;
            right: -450px;
            width: 450px;
            color: red;
            float: right;
        }

    </style>
</head>
<body>
<div class="container">
    <h1>Форма обратной связи</h1>
    <p><a href="/parse">На главную</a> / <a href="/contact/messages">Комментарии</a></p>
    <form action="/contact/saveMessage" method="post" class="form-horizontal" id="form_bwt">
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Введите имя</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" value="<?php echo ($_SESSION[ 'userCaptha' ]) ? : '' ?>"
                       name="name" id="name" placeholder="Имя" required>
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Введите email</label>
            <div class="col-sm-5">
                <input type="email" class="form-control" id="email"
                       value="<?php echo ($_SESSION[ 'emailCaptha' ]) ? : '' ?>" name="email" required
                       placeholder="Email">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-5">
                <textarea name="ctextarea" class="form-control" required rows="5" maxlength="2048"
                          placeholder="Введите сообщение"><?php echo ($_SESSION[ 'massageCaptha' ]) ? : '' ?></textarea>
                <br>
            </div>
            <div class="col-sm-offset-2 col-sm-5">
                <img src="../components/noise-picture.php">
            </div>
            <div class="col-sm-offset-2 col-sm-5">
                <input type="text" name="answer" size="6" style="width: 210px; border-radius: 5px">
            </div>
            <div class="col-sm-offset-2 col-sm-5">
                <button type="submit" class="btn btn-default">Отправить сообщение</button>
            </div>
        </div>
    </form>
    <h1>
		<?php
		echo $_SESSION[ 'captha_res' ];
		$_SESSION[ 'captha_res' ] = null;
		?>
    </h1>
</div>
<script src="../style/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        $("#form_bwt").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 20
                },
                ctextarea: {
                    required: true,
                    minlength: 2,
                    maxlength: 255
                }
            },
            messages: {
                name: {
                    required: "Это поле обязательно для заполнения",
                    minlength: 'Введите имя от 2 до 20 символов',
                    maxlength: 'Введите имя от 2 до 255символов'
                },
                ctextarea: {
                    required: "Это поле обязательно для заполнения",
                    minlength: 'Введите сообщение от 2 до 255 символов',
                    maxlength: 'Введите сообщение от 2 до 255 символов'
                },
                email: {
                    required: "Это поле обязательно для заполнения",
                    email: "Введите корректный email например abc@gmail.com"
                }
            },
            errorElement: "div",
            errorPlacement: function (error, element) {
                console.log(error);
                console.log(element);
                error.insertAfter(element);
            },

        });
        $("#radio").buttonset();
    });
</script>
</body>
</html>