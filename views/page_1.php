<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link href="../style/_css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../style/_css/jquery-ui.min.css">
    <style>
        .radio {
            margin-left: 20px;
        }

        form div {
            margin: 0 0 10px 0;
            position: relative;
        }

        p.label {
            font-size: 1.5em;
        }

        select {
            color: black;
        }

        .col-sm-4 p {
            height: 50px;
        }
    </style>
</head>
<body>
<div class="container">
    <p><a href="/contact">Сообщения</a> / <a href="/contact/messages">Комментарии</a></p>
	<?php if (!$_SESSION[ 'userName' ]) : ?>
        <div class="row">
            <div class="col-sm-6">
                <h1>Войти</h1>
                <form accept-charset="UTF-8" class="form-horizontal" role="form" action="/parse/authForm" method="post"
                      id="form_bwt_inside">
                    <div class="form-group">
                        <label for="name_inside"></label>
                        <div class="col-sm-6">
                            <input class="form-control" type="text" id="name_inside" name="name" required
                                   placeholder="Введите имя"><br>
                            <input type="submit" id="submit_inside" class="btn btn-default" value="Войти">
							<?php if ($_SESSION[ 'error_name' ]) : ?>
                                <h2 id="error_name"><?= $_SESSION[ 'error_name' ] ?></h2>
								<?php $_SESSION[ 'error_name' ] = null; ?>
							<?php endif; ?>
                        </div>
                    </div>
                </form>
            </div><!--end col-sm-6-->
            <div class="col-sm-6">
                <p><a href="/weather"><img src="../style/images.jpg" alt="" width="300" height="100"></a></p>
            </div><!--end col-sm-6-->
        </div>
	<?php else: ?>
        <div class="row">
            <div class="col-sm-6">
                <h2>Привет <?= $_SESSION[ 'userName' ] ?></h2>
                <h6><a href="/parse/out">Выход</a></h6>
            </div>
            <div class="col-sm-6">
                <p><a href="/weather"><img src="../style/images.jpg" alt="" width="300" height="100"></a></p>
            </div><!--end col-sm-6-->
        </div>
	<?php endif; ?>
    <div class="row">
        <div class="col-sm-6">
            <h1 id="register"><a href="#"> Регистрация</a></h1>
            <div id="showRegister" style="display: none">
                <form class="form-horizontal" role="form" action="/parse/saveForm" method="post" id="form_bwt">
                    <div class="form-group">
                        <label for="name"></label>
                        <div class="col-sm-6">
                            <input class="form-control" type="text" id="name" name="name" required
                                   placeholder="Введите имя">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="family"></label>
                        <div class="col-sm-6">
                            <input class="form-control" type="text" id="family" name="family" required
                                   placeholder="Введите фамилию">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cemail"></label>
                        <div class="col-sm-6">
                            <input class="form-control" type="email" id="cemail" name="email" required
                                   placeholder="Введите email">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="radio">
                            <label>
                                <input type="radio" name="sex" id="male" value="1">
                                Mужской
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="sex" id="female" value="2">
                                Женский
                            </label>
                        </div>
                    </div><!--end form-group-->
                    <div class="form-group">
                        <div class="col-sm-6">
                            <input class="form-control" type="text" id="birthday" name="birthday"
                                   placeholder="Введите дату рождения">
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" id="submit" class="btn btn-default">
                    </div>
                </form>
            </div><!--end showRegister-->
        </div><!--end col-sm-6-->
    </div><!--end row-->
</div><!--end container-->
<div id="result"><?php
	if ($_SESSION[ 'OK' ]) {
		echo "<p> Поздравляем " . $_SESSION[ 'userName' ] . " вы зарегистрированы</p>";
	}
	$_SESSION[ 'OK' ] = null;
	?>
</div>
<!--<pre>-->
<!--    --><? //print_r($this->getUsersName()) ?>
<!--</pre>-->
<script src="../style/js/jquery.js"></script>
<script src="../style/js/bootstrap.min.js"></script>
<script src="../style/js/jquery-ui.min.js"></script>
<script src="../style/js/jquery.validate.min.js"></script>
<script>
    $(document).ready(function () {
        $('#register a').click(function () {
            $("#showRegister").toggle();
        });
        $("#result").hide(4000);
        $("#birthday").datepicker({
            changeYear: true,
            changeMonth: true,
            buttonText: 'Выбери дату рождения',
            yearRange: "-120:+0",
            dateFormat: 'dd/mm/yy',
            defaultDate: "1/1/1990"
        });
        $("#form_bwt_inside").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 20
                }
            },
            messages: {
                name: {
                    required: "",
                    minlength: '',
                    maxlength: ''
                }
            },
            errorClass: 'has-error',
            validClass: 'has-success',
            highlight: function (element, errorClass, validClass) {
                $(element).parents("div.col-sm-6").addClass(errorClass).removeClass(validClass);

            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents("div.has-error").removeClass(errorClass).addClass(validClass);
            }
        });
        $("#form_bwt").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 20
                },
                family: {
                    required: true,
                    minlength: 2,
                    maxlength: 20
                }
            },
            messages: {
                name: {
                    required: "",
                    minlength: '',
                    maxlength: ''
                },
                family: {
                    required: "",
                    minlength: '',
                    maxlength: ''
                },
                email: {
                    required: "",
                    email: ""
                }
            },
            errorClass: 'has-error',
            validClass: 'has-success',
            highlight: function (element, errorClass, validClass) {
                $(element).parents("div.form-group").addClass(errorClass).removeClass(validClass);

            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents("div.has-error").removeClass(errorClass).addClass(validClass);
            }
        });
    });
</script>
</body>
</html>