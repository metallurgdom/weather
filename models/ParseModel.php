<?php
namespace models;
use components\Service;
class ParseModel extends DbModel{
    public $data;
    public function __construct(){
        parent::__construct();
    }
    public function render($file){
    	
        ob_start();
        include(dirname(__FILE__).'/'.$file);
        return ob_get_clean();
    }
    public function setDataForm($data){}

    public function saveRegisterToDB($data){
        $name=NULL; $family=NULL; $birthday='01/01/2000'; $sex=1;
        $name = Service::clearStr($data['name']);
        $family = Service::clearStr($data['family']);
        $email = Service::clearStr($data['email']);
        if(preg_match('#^\d{1,2}\/\d{1,2}\/\d{2,4}$#', $data['birthday']))
        $birthday = $data['birthday'];
        $sex = Service::clearInt($data['sex']);
        $sql = "INSERT INTO users (name, family, email, sex, birthday)
                VALUES (:name, :family, :email, :sex, :birthday)";
        $stmt = $this->db->prepare($sql) or die('Ошибкa в запросе!');
        $stmt->bindValue(':name', $name);
        $stmt->bindValue(':email', $email);
        $stmt->bindValue(':family', $family);
        $stmt->bindValue(':birthday', $birthday);
        $stmt->bindValue(':sex', $sex);
        $stmt->execute();
    }

}