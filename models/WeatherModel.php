<?php
namespace models;

class WeatherModel extends DbModel{
    public $data;
    public function __construct(){
        parent::__construct();
    }
    public function render($file){
        ob_start();
        include(dirname(__FILE__).'/'.$file);
        return ob_get_clean();
    }
    public function __set ( $name , $value ){
        $this->$name = $value;
    }
}