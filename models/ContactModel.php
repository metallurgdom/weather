<?php
namespace models;

use components\Service;

class ContactModel extends DbModel{
    public $data;
    public function __construct(){
        parent::__construct();
        $a=20;
    }
    public function render($file){
        ob_start();
        include(dirname(__FILE__).'/'.$file);
        return ob_get_clean();
    }

    public function captha(){
        $img = imagecreatefromjpeg('style/noise.jpg');
        imageAntiAlias( $img, true );
        $str = '';
        $test = substr( md5( uniqid() ), 0, 5 );
        for ( $i = 0, $x = 20, $y = 30; $i < 5; $i ++ ) {
            $upper = rand( 0, 1 );
            ( $upper == 1 ) ? $char = $this->randChar() : $char = mb_strtolower( $this->randChar() );
            $color = (int) ( $this->random_color() );
            imageTtfText( $img, rand( 24, 30 ), rand( - 40, 40 ), $x, $y, $color, 'style/font/bellb.ttf', $test{$i} );
            $x += 40;
        }
        header("Content-type: image/jpg");
        imageJpeg($img);
    }

    public function getMessages(){
        $sql = "SELECT * FROM messages";
        $res = $this->db->query($sql);
        $messages = $res->fetchAll(\PDO::FETCH_ASSOC);
        return $messages;
    }
    public function saveMessageToDB($data){
        $name=NULL; $message = NULL;
        $name = Service::clearStr($data['name']);
        $message = Service::clearStr($data['ctextarea']);
        $email = Service::clearStr($data['email']);
        $sql = "INSERT INTO messages (name, email, messages)
                VALUES (:name, :email, :messages)";
        $stmt = $this->db->prepare($sql) or die('Ошибкa в запросе!');
        $stmt->bindValue(':name', $name);
        $stmt->bindValue(':email', $email);
        $stmt->bindValue(':messages', $message);
        $stmt->execute();
    }

}