<?php
namespace models;
class News{
    public static function getNewsItemById( $id ){
        $id=intval($id);
        if($id){
            $db = Db::getConnection();
            $result = $db->query('SELECT id, title FROM news WHERE id='.$id);
            //$result->setFetchMode(PDO::FETCH_NUM);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $newList = $result->fetch();
            return $newList;
        }

    }

    public static function getNewsList(){
        $db = Db::getConnection();
        $newList=[];

        $result = $db->query('SELECT id, title FROM news ORDER BY title DESC LIMIT 3');
        $i=0;
        while($row = $result->fetch()){
            $newList[$i]['id']=$row['id'];
            $newList[$i]['title']=$row['title'];
            $i++;
        }
        return $newList;


    }
}