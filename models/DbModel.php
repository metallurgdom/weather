<?php
namespace models;
error_reporting(1);
class DbModel{
    /*
     * @var object A database object
     * */
    protected $db;
    const DB_NAME = 'weather.db';

    protected function __construct($db=NULL){
        try {
            $this->db = new \PDO('sqlite:'.'db/'.self::DB_NAME);
            $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING);
            if( is_file('db/'.self::DB_NAME) and filesize('db/'.self::DB_NAME) == 0 ){
                    $sql = 'CREATE TABLE users(
                              id INTEGER PRIMARY KEY AUTOINCREMENT,
                              name TEXT,
                              family TEXT,
                              email TEXT,
                              sex TEXT,
                              birthday TEXT
                            )';
                    $this->db->exec($sql);
                    $sql = 'CREATE TABLE messages(
                              id INTEGER PRIMARY KEY AUTOINCREMENT,
                              name TEXT,
                              email TEXT,
                              messages TEXT
                            )';
                    $this->db->exec($sql);
            }
        }catch (\PDOException $e){
            $e->getMessage();
        }
    }
    function __destruct(){
        unset($this->db);
    }
    public function getUsersName(){
        $sql = "SELECT name FROM users";
        $res = $this->db->query($sql);
        $users = [];
        while($arr = $res->fetch(\PDO::FETCH_ASSOC)){
            $users[] = $arr['name'];
        }
        return $users;
    }

}