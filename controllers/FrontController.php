<?php
namespace controllers;
error_reporting(0);
class FrontController{
    protected $_controller, $_action, $_params, $_body;
    static $_instance;
    public static function getInstance(){
        if( !(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    private function __clone(){}
    private function __construct(){
        $request = $_SERVER['REQUEST_URI'];
        $splits = explode('/', trim($request , '/'));
        //controller
        $this->_controller = !empty($splits[0]) ?
            ucfirst($splits[0]).'Controller' : 'IndexController';
        //Action
         $this->_action = !empty($splits[1]) ? $splits[1].'Action' : 'indexAction';
        //params
        if(!empty($splits[2])){
            $this->_params = $splits[2];
        }
    }

    public function route(){
		$c = 'controllers\\'.$this->getController();
        if(class_exists('controllers\\'.$this->getController())){
            $rc = new \ReflectionClass($c);
                if($rc->hasMethod($this->getAction())){
                    $controller = $rc->newInstance();
                    $method = $rc->getMethod($this->getAction());
                    $method->invoke($controller);
                }else{
                    throw new Exception("Action");
                }
        }else{
            $this->_controller = 'IndexController';
                $rc = new ReflectionClass($this->getController());
                $controller = $rc->newInstance();
                $method = $rc->getMethod('indexAction');
                $method->invoke($controller);
//            throw new Exception("Controller");
        }
    }

    public function getParams() {
        return $this->_params;
    }
    public function getController() {
        return $this->_controller;
    }
    public function getAction() {
        return $this->_action;
    }
    public function getBody() {
        return $this->_body;
    }
    public function setBody($body) {
        $this->_body = $body;
    }
    
}