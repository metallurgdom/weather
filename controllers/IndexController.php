<?php
namespace controllers;
use models\ParseModel;
class IndexController{
    public function indexAction(){
        $front = FrontController::getInstance();
        $model = new ParseModel();
        $res = $model->render("../views/page_1.php");
        $front->setBody($res);
    }

}