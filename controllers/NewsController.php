<?php
include_once ROOT.'/models/News.php';
class NewsController{
    public function actionIndex(){
        $newsList =[];
        $newsList = News::getNewsList();
        //print_r($newsList);
        require_once(ROOT.'/views/news/index.php');
        return true;
    }

    public function actionView($id){
        if($id){
            $newItem = News::getNewsItemById($id);
            print_r($newItem);
        }
        return true;
    }
}