<?php
namespace controllers;

use models\ContactModel;
use components\Service;

class ContactController
{
	public function indexAction()
	{
		
		$front = FrontController::getInstance();
		$model = new ContactModel();
		$res = $model->render("../views/page_3.php");
		$front->setBody($res);
	}
	
	public function messagesAction()
	{
		$front = FrontController::getInstance();
		$model = new ContactModel();
		$res = $model->render("../views/page_4.php");
		$front->setBody($res);
	}
	
	public function saveMessageAction()
	{
		session_start();
		$model = new ContactModel();
		$answer_out = $_SESSION[ 'captha' ];
		if ($_POST[ 'answer' ] == $answer_out and !empty($answer_out) and !empty($_POST[ 'answer' ])) {
			$_SESSION[ 'captha_res' ] = 'Сообщение добавлено';
			unset($_SESSION[ 'userCaptha' ]);
			unset($_SESSION[ 'massageCaptha' ]);
			unset($_SESSION[ 'emailCaptha' ]);
			$model->saveMessageToDB($_POST);
			header("Location: " . PATH . 'contact');
			exit;
		} else {
			$_SESSION[ 'captha_res' ] = 'Повторите проверку';
			$_SESSION[ 'userCaptha' ] = (Service::clearStr($_POST[ 'name' ]));
			$_SESSION[ 'massageCaptha' ] = (Service::clearStr($_POST[ 'ctextarea' ]));
			$_SESSION[ 'emailCaptha' ] = (Service::clearStr($_POST[ 'email' ]));
			header("Location: " . PATH . 'contact');
			exit;
		}
		
	}
}