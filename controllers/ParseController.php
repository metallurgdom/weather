<?php
namespace controllers;
use models\ParseModel;
use components\Service;

class ParseController{
    public function indexAction(){
        $front = FrontController::getInstance();
        $model = new ParseModel();
        $res = $model->render("../views/page_1.php");
        $front->setBody($res);
    }
    public function outAction(){
	    $_SESSION['userName'] = '';
        header("Location: ". PATH."parse");
    }
    public function authFormAction(){
	   
        session_start();
        $model = new ParseModel();
        $usersName = $model->getUsersName();
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $name = Service::clearStr($_POST['name']);
            if(in_array($name, $usersName)){
//                setcookie("userName", $name, time() + 2*60*60,'/');
                $_SESSION['userName'] = $name;
            }else{
                $_SESSION['error_name'] = 'Не верное имя';
            }
        }
        header("Location: ". PATH."parse");
        exit();
    }
    public function saveFormAction(){
        session_start();
        $model = new ParseModel();
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $model->saveRegisterToDB($_POST);
            header("Location: " . PATH.'parse');
            $_SESSION['OK'] = 1;
            $_SESSION['userName'] = Service::clearStr($_POST['name']);
        }else{
            header("Location: " . PATH.'parse');
        }
    }
}