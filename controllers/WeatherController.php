<?php
namespace controllers;

use models\WeatherModel;
use components\Service;

class WeatherController{
	public $access = false;
	public function indexAction()
	{
		if (isset($_SESSION['userName'])) {
			$name = Service::clearStr($_SESSION['userName']);
			$usersName = (new WeatherModel())->getUsersName();
			if (in_array($name, $usersName)) {
				$this->access = true;
			}
		}
        $url = 'https://www.gismeteo.ua/weather-zaporizhia-5093/';
        $html = $this->curl_get($url);
        $pos = strpos($html,'weather-daily');
        $html = substr($html, $pos);
        $pos_end = strpos($html,'section bottom');
        $html = substr($html, 0, $pos_end);
        preg_match_all('#<img.*src="(.*?)"#', $html, $img_top);
        preg_match_all('#<div class="temp"><span class=\'value m_temp c\'>([+-.\d]+)</span>#',
            $html, $temp_top_1);//+19...+35
        preg_match_all('#<em><span class=\'value m_temp c\'>(.+)</span><#',
            $html, $temp_top_2);//+19...+35
        preg_match_all('#<dt>([А-Я]+)</dt><dd>(\d+.\d+)</dd>#',
            $html, $date_day_top);//2.08 ВТ

        /*table row 1*/
        preg_match_all('#class="clicon"><img .*?src="([^"]+)#',
            $html, $row_weather_img);//ЯСНО img
        preg_match_all('#<td class="cltext">(.*)</td>#',
            $html, $row_text);//ЯСНО
        preg_match_all('#td class=[\'\"]temp[\'\"]><span .*?value m_temp c.*?>([+-]\d+)#',
            $html, $row_temp);//+22
        preg_match_all('#<td><span class=[\'\"]value m_press torr[\'\"]>([+-]?\d+)</span>#',
            $html, $row_pressure);//давление
        preg_match_all('#(<dt class=[\'"]wicon wind\d[\'"].*?class=[\'"]value m_wind ms[\'"]>\d+</span>)#',
            $html, $row_wind);//ветер
        preg_match_all('#<td>(\d+)</td>#',
            $html, $row_humidity);//влажность
        preg_match_all('#<td><span class=[\'"]value m_temp c[\'"]>([+-]\d+)</span>#',
            $html, $row_feeling);//ощущается <td><span class='value m_temp c'>

        $front = FrontController::getInstance();
        $model = new WeatherModel();
        $model->img_top = $img_top;
        $model->date_day_top = $date_day_top;
        $model->temp_top_1 = $temp_top_1;
        $model->temp_top_2 = $temp_top_2;
        $model->row_weather_img = $row_weather_img;
        $model->row_text = $row_text;
        $model->row_temp = $row_temp;
        $model->row_pressure = $row_pressure;
        $model->row_wind = $row_wind;
        $model->row_humidity = $row_humidity;
        $model->row_feeling = $row_feeling;
        $res = $model->render("../views/page_2.php");
        $front->setBody($res);
    }

    private function curl_get($url, $referer = 'http://www.google.com'){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT,
            "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0");
        curl_setopt($ch, CURLOPT_REFERER, $referer);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

}